
`.@extends('layouts.app')
@section('content')

<br>
<div id="page-wrapper">
    <div class="row">
        <div style="width:70%;" class="cart">
           <!--  <div id="custom-search-input" style="width:550px;margin-left: 365px;float:left;">
                <div class="input-group col-md-12">
                    <input type="text" class="  search-query form-control" placeholder="Search" />
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">
                            <span class=" glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </div> -->
            <div style="margin-left:1250px;width:100px;">
                @if(Session::has('cart'))           
                    <a href="{{ URL::to('cart')}}" class="glyphicon glyphicon-shopping-cart">Cart({{ $cart_qty }})</a>
                @else 
                    <a href="{{ URL::to('cart')}}" class="glyphicon glyphicon-shopping-cart">Cart(0)</a>    
                @endif
            </div>
        </div>
        <div class="col-md-3 sidemenu"> 
            <div class="nav-side-menu">
                <div class="brand">Categories</div>
                <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
                <div class="menu-list">
                    <ul id="menu-content" class="menu-content collapse out">
                        @foreach($categories as $category)
                        <li data-toggle="collapse" data-target="#{{ str_replace(' ', '', $category->cat_name ) }}" class="collapsed"  aria-expanded="false">
                            <a href="{{ URL::to('category/'.$category->id )}}"><i class="fa fa-gift fa-lg"></i>{{ $category->cat_name }}</a><span class="{{ count($category->childs) != '' ? 'arrow' : '' }}"></span>
                        </li>
                            @if(count($category->childs))
                                <ul class="collapse first" id="{{ str_replace(' ', '', $category->cat_name ) }}">
                                    @include('managechild',['children' => $category->childs])
                                </ul>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="col-md-9 main-menu" style="width:70%;">
            <div class="row"> 
                @foreach($products as $product)
                    <div class="col-lg-3 col-md-6" style="width: 18%!important">                   
                        <div style="margin-bottom: 5px;">
                            <a href="{{ URL::to('product/'.$product->id)}}">
                                @if(file_exists('../public/photos/'.$product->photos[0]))
                                  <img src="{{asset('../public/photos/'.$product->photos[0])}}" class="image" width="150" height="190"/>
                                @else
                                  <img src="{{asset('../public/photos/cover.jpg')}}" width="150" class="image" height="190" />
                                @endif
                                <!-- <img src="{{asset('../public/photos/'.$product->photos[0])}}" width="200" height="250"/> -->
                            </a>                          
                        </div>
                        <div>
                            <a href="<?= URL::to('session/'.$product->id) ?>" class="btn btn-primary" style="clear: both;border-radius: 0px!important;width: 150px;">Add To Cart</a>
                            <br>
                        </div>

                        <div class="" style="text-align: left;">
                            @if(file_exists('../public/pdf/'.$product->pdf))
                                <a href="{{asset('../public/pdf/'.$product->pdf)}}" target="_blank" style="text-decoration: none;"><i>Sample PDF</i></a>
                            @endif
                            <br>
                            <b style="color: #23256C;">{{ $product->p_name }}</b>
                            <br>
                            <p style="color: #23256C;">{{ $product->price }} kyats</p>
                        </div>                
                        <br>                  
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
