@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" style="margin-top: 70px !important;">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="panel-heading" style="font-size: 18px;">
                @if(isset($orders))
                    OrderList
                @elseif(isset($userorders))
                    MyOrderList
                @endif
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <th>CustomerName</th>
                            <th>Total</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    @if(isset($orders))
                        <tbody>
                        	@foreach($orders as $order)
    	                        <tr class="odd gradeX">                                
    	                            <td><a href="{{ URL::to('admin/order/'.$order->id)}}">{{$order->user->name}}</a></td>
                                    <td>{{$order->total}}</td>
    	                            <td>{{$order->date}}</td>
    	                        </tr>
    	                    @endforeach
    	                </tbody>
                    @elseif(isset($userorders))
                        <tbody>
                            @foreach($userorders as $order)
                                <tr class="odd gradeX">                                
                                    <td><a href="{{ URL::to('member/order/'.$order->id)}}">{{$order->user->name}}</a></td>
                                    <td>{{$order->total}}</td>
                                    <td>{{$order->date}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    @endif
	            </table>
                
	        </div>
            @if(isset($orders))
                {{ $orders->links()}}
            @elseif(isset($userorders))
                {{ $userorders->links() }}
            @endif
	    </div>
	</div>
</div>
@endsection