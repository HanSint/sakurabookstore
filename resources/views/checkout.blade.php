@extends('layouts.app')
@section('content')

<div style="margin-left:1250px;">
    @if(isset($carts_qty))
          
        <a href="{{ URL::to('member/cart')}}" class="glyphicon glyphicon-shopping-cart">Cart({{ $carts_qty }})</a> 
       
    @else
       
        <a href="{{ URL::to('member/cart')}}" class="glyphicon glyphicon-shopping-cart">Cart(0)</a>
        
    @endif
</div>
<div class="container">
	<div class="row">
		<div class="modal-content" style="margin-top: 70px !important;">
			@if ($message = Session::get('success'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
			@endif
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<form action="{{ URL::to('member/order')}}" method="post">
			{{ Form::open(['url' => '/member/order', 'method' => 'post' ]) }}
			
				<div class="modal-header">
					<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button> -->
					<h3 class="modal-title">OrderItems</h3>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<table class="table table-striped table-bordered table-hover" id="dataTables">
							<tbody>
								
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit Price</th>
									<th>Total Price</th>
								</tr>
								@foreach($carts as $cart)
									<tr>
										<input type="hidden" name="pid[]" value="{{$cart->product_id}}">
										<td id="pname" name="pname" ><img src="{{asset('photos/'.$cart->product->photos[0])}}" width="70" height="70"/> {{ $cart->product->p_name}}</td>

										<td id="qty" >
											{{$cart->qty}}
											<input type="hidden" name="qty[]" style="width: 45px; padding: 1px" value="{{$cart->qty}}" min="1">
										</td>
										<td id="price">
											<p>{{ $cart->product->price }}</p>
											<input type="hidden" name="price[]" value="{{ $cart->product->price }}">
										</td>
										<td style="width:300px;" class="pricetotal" id="pricetotal">
											{{ $cart->product->price*$cart->qty }}
											<input type="hidden" name="pricetotal" class="pricetotal" id="pricetotal" value="{{ $cart->product->price*$cart->qty }}">
										</td>
									</tr>
								@endforeach
								<tr>
									<td colspan="3"><span class="pull-right">Sub Total</span></td>
									<td name="subtotal" id="subtotal">
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<span class="pull-right">Shipping
										<select id="town" onchange="township()">
											<option value="2000">Bahan</option>
											<option value="2000">Tamwe</option>
											<option value="2000">Pansodan</option>
											<option value="2000">Kyoutdadar</option>
											<option value="2500">Tharkayta</option>
											<option value="2500">Kamaryut</option>
											<option value="2500">Sanchuang</option>
											<option value="3500">HlaungTharYar</option>
											<option value="2500">Mayangone</option>
											<option value="2000">Shwegonedi</option>
											<option value="3000">Other</option>
										</select>
										</span>
										<input type="hidden" class="shipping" name="shipping" value="">
									</td>
									<td id="shipping">			
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<span class="pull-right">Tax (10%)</span>
										<input type="hidden" class="tax" name="tax" value="">
									</td>
									<td id="tax">	
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<span class="pull-right">Total</span>
										<input type="hidden" class="total" name="total" value="">
									</td>
									<td id="total">
									</td>
								</tr>
								
										 
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="modal-header">
					<h3 class="modal-title">Shipping Info</h3>
				</div>
				
				<div class="modal-body">
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
			
								{{ Form::label('', 'Name') }}
								{{ Form::text('name','', ['class' => 'form-control',  'required' =>'true']) }}
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('','Billing') }}
								{{ Form::text('billing','',['class'=> 'form-control', 'required' => 'true'])}}
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
  
								{{ Form::label('','Delivery Address')}}
								{{ Form::text('address','',['class' => 'form-control', 'required'=> 'true'])}}
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('','Phone') }}
								{{ Form::text('phone','',['class'=> 'form-control', 'required' => 'true'])}}
								
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<a href="{{URL::previous() }}" class="btn btn-info btn-icon" style="margin-right:980px;"></i>Cancel</a>
					{{ Form::submit('Order',['class' => 'btn btn-success btn-icon'])}}
				</div>

			{!! Form::close() !!}
		</div>
	</div>
</div>
<script type="text/javascript">

</script>
@endsection