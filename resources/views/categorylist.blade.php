@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
    	<div  style="margin-top: 70px !important;">
    		@if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
    		@if ($message = Session::get('success'))
			    <div class="alert alert-success alert-block">
			        <button type="button" class="close" data-dismiss="alert">×</button> 
			            <strong>{{ $message }}</strong>
			    </div>
			@endif
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading" style="font-size: 16px;">Category list</div>
	                <div class="panel-body">
	                    <!-- <form class="form-horizontal" role="form" method="POST" action="<?= URL::to('admin/cat_create')?>"> -->
	                        {{ csrf_field() }}

	                        <div class="form-group">		    
							    <div class="col-md-6">

							        @foreach($categories as $category)
						                <ul>
						                    <li><form method="post" action="{{URL::to('admin/category/'.$category->id)}}">{{csrf_field()}}{{ $category->cat_name }} <input type="hidden" name="_method" value="DELETE" ><button type="submit" class="btn btn-warning btn-sm" onclick="return confirm('Are You Sure Want To Delete?');"> Del </button>  <a href="{{URL::to('admin/category/'.$category->id)}}" class="btn btn-primary btn-sm">Edit</a> </form></li> 
						                     <br>
						                    @if(count($category->childs))
						                        @include('managechild',['childs' => $category->childs])
						                    @endif
						                </ul>
						            @endforeach
							     
							    </div>
							</div>
						<!-- </form> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection