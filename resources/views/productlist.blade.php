@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" style="margin-top: 70px !important;">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="panel-heading" style="font-size: 16px;">
                ProductList
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Photo</th>
                            <th>Price</th>
                            <th>Quentity</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($products as $product)
	                        <tr class="odd gradeX">
	                            <td><a href="{{ URL::to('admin/product_detail/'.$product->id)}}">{{$product->p_name}}</a></td>
                                <td>                                        
                                    <img src="{{asset('photos/'.$product->photos[0])}}" width="70" height="80"/> 
                                </td>                                     
                                <td>{{$product->price}}</td>
	                            <td>{{$product->qty}}</td>
	                            <td><a href="{{ URL::to('admin/product/'.$product->id)}}" class="btn btn-info">Edit</a></td>
                                <td>
                                    <form method="post" action="{{ URL::to('admin/product/'.$product->id)}}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    	                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are You Sure Want To Delete?');">Delete</button>
    	                            </form>
                                </td>
                            </tr>
	                    @endforeach
	                </tbody>
	            </table>
                
	        </div>
            {{$products->links()}}
	    </div>
	</div>
</div>
@endsection