@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" style="margin-top: 70px !important;">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="panel-heading">
                CartView
            </div>
             @if( Auth::user() )
                    <div style="margin:left;margin-bottom: -11px;">
                            
                            <form method="post" action="{{ URL::to('cart')}}">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <button type="submit" value="delete" style="margin-left: 20px;margin-top: 20px;" class="btn" onclick="return confirm('Are You Sure Want To Remove All Items?');">All Clear</button>                   
                            </form>

                            <a href="{{ URL::to('member/checkout')}}" style="margin-left:1060px;margin-top: -59px;" class="btn btn-success">Checkout</a>
                             <a href="{{URL::to('member/home')}}" style="margin-top: -59px;" class="btn btn-primary">Continue Shopping</a>
                            
                        </div>
                @else
                    <div style="margin:left;margin-bottom: -11px;">
  
                        <form method="post" action="{{ URL::to('session')}}">
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
                            <button type="submit" value="delete" style="margin-left: 20px;margin-top: 20px;" class="btn" onclick="return confirm('Are You Sure Want To Remove All Items?');">All Clear</button>                   
                        </form>
                        <a href="{{URL::to('index')}}" style="margin-left:1060px;margin-top: -59px;" class="btn btn-primary">Continue Shopping</a>
                        <a href="{{ URL::to('login')}}" style="margin-top: -59px;" class="btn btn-success">Checkout</a>
                        
                    </div>
                @endif                
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Photo</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Operation</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(isset($carts))
                    	@foreach($carts as $cart)
	                        <tr class="odd gradeX">
	                            <td>{{$cart->product->p_name}} <p></p>
                                    <form method="post" action="{{ URL::to('cart/'.$cart->id)}}">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button type="submit" value="delete" class="alink" onclick="return confirm('Are you sure want to remove this item?');">Remove</button>
                                        
                                    </form>
                                </td>
                                <td>                                        
                                    <img src="{{asset('photos/'.$cart->product->photos[0])}}" width="50" height="50"/> 
                                </td>                                     
                                <td name="price[]" id="price">
                                    <p>{{$cart->product->price}}</p>
                                </td>
                                <td>
                                    <input type="hidden" class="url" id="product" value="{{ URL::to('cart/'.$cart->product_id)}}">
                                    <input type="number" name="qty[]" id="qty" style="width: 45px; padding: 1px" value="{{$cart->qty}}" min="1" onchange="change(this)">
                                </td>
	                            <td id="pricetotal" class="pricetotal">
                                    {{ $cart->product->price * $cart->qty }}
                                </td>
	                        </tr>
	                    @endforeach
                        
                    @elseif(isset($products))
                   
                        @for($i=0; $i< count($products); $i++)
                            
                            <tr class="odd gradeX">

                                <td>{{$products[$i]->p_name}} <p></p>  
                                   <form method="post" action="{{ URL::to('session/'.$i)}}">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button type="submit" class="alink" value="delete" onclick="return confirm('Are you sure want to remove this item?');">Remove</button>    
                                    </form>
                                </td>
                                <td>                                        
                                    <img src="{{asset('../public/photos/'.$products[$i]->photos[0])}}" width="50" height="50"/> 
                                </td>                                     
                                <td class="price" id="price">
                                    <p>{{$products[$i]->price}}</p>
                                </td>
                                <td>
                                    <input type="hidden" class="url" id="product" value="{{ URL::to('session/'.$products[$i]->id)}}">
                                    <input type="number" name="qty[]" id="qty" style="width: 45px; padding: 1px" value="{{$products[$i]->order_qty}}" min="1" onchange="change(this)">
                                </td>
                                <td id="pricetotal" class="pricetotal">
                                    {{$products[$i]->price * $products[$i]->order_qty}}
                                </td>
                            </tr>

                        @endfor

                    @endif
                        <tr class="odd gradeX">
                            <td colspan="4"><p align="right">Total</p></td>
                            <td name="subtotal" id="subtotal"></td>
                        </tr>
	                </tbody>
	            </table>
               

	        </div>
	    </div>
	</div>
</div>
<script type="text/javascript">

    

    function change($this)
    {   var tr = $($this).parent().closest('td');
        var url = tr.find('input.url').val();
        var qty = $($this).val();
        // var price = tr.prev('td').text(); 
        // var item_total = qty * price;
        // tr.next().text(item_total);

        $.ajax({
            url: url,
            type: 'get',
            data: {qty: qty},
            dataType: 'json',
            success: function (response) {
                console.log(response);
            }
        });
    }
</script>
@endsection
