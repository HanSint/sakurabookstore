@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div  style="margin-top: 70px !important;">
            @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="font-size: 16px;">Profile Edit</div>
                    <div class="panel-body">
                        @if(isset($customer))
                            {{ Form::open(['url' => 'member/profile/'.Auth::user()->id , 'method' => 'post', 'class'=>'form-horizontal']) }}
                  
                                {{ Form::hidden('_method','PUT') }}
                                {{ Form::hidden('_token', csrf_token() ) }}
                
                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                    {{ Form::label('', 'Username',[ 'class' => 'col-md-4 control-label' ]) }}
                                    
                                    <div class="col-md-6">
                                        {{ Form::text('name', Auth::user()->name, ['class' => 'form-control', 'required' => 'true']) }}

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    {{ Form::label('', 'E-Mail Address',[ 'class' => 'col-md-4 control-label' ]) }}

                                    <div class="col-md-6">
                                        {{ Form::email('email', Auth::user()->email, ['class' => 'form-control', 'required' => 'true']) }}

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{ Form::label('', 'Date of Birth',[ 'class' => 'col-md-4 control-label' ]) }}
                                    <div class="col-md-6">
                                        {{ Form::date('birthday', $customer->birth, ['class' => 'form-control', 'required' => 'true']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{ Form::label('', 'Address',[ 'class' => 'col-md-4 control-label' ]) }}
                                    <div class="col-md-6">
                                        <textarea type="address" name="address" id="address" placeholder="Address" class="form-control">{{ $customer->address }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{ Form::hidden('usertype', 0, ['class' => 'form-control', 'required' => 'true']) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('', 'Phone',[ 'class' => 'col-md-4 control-label' ]) }}
                                    <div class="col-md-6">
                                       {{ Form::text('phone', $customer->phone, ['class' => 'form-control', 'required' => 'true']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{ Form::label('', 'Gender',[ 'class' => 'col-md-4 control-label' ]) }}

                                    @if( $customer->gender === 'male')
                                        <div class="col-md-6"> 
                                            <label class="radio-inline" for="male">
                                                <input type="radio" name="gender" id="male" value="male" checked>
                                              Male
                                            </label> 
                                            <label class="radio-inline" for="female">
                                                <input type="radio" name="gender" id="female" value="female">
                                              Female
                                            </label>
                                        </div>
                                    @else
                                        <div class="col-md-6"> 
                                            <label class="radio-inline" for="male">
                                                <input type="radio" name="gender" id="male" value="male" >
                                              Male
                                            </label> 
                                            <label class="radio-inline" for="female">
                                                <input type="radio" name="gender" id="female" value="female" checked>
                                              Female
                                            </label>
                                        </div>
                                    @endif
                                </div>  
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        {{ Form::submit('Update', ['class' => 'btn btn-primary']) }}
                                        <a href="{{ URL::to('home')}}" class="btn btn-info">
                                            Cancel
                                        </a>

                                    </div>
                                </div> 
                            {{ Form::close() }}
                        @elseif(isset($admin))
                            {{ Form::open(['url' => 'admin/profile/'.Auth::user()->id , 'method' => 'post', 'class'=>'form-horizontal']) }}
                  
                                {{ Form::hidden('_method','PUT') }}
                                {{ Form::hidden('_token', csrf_token() ) }}
                
                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                    {{ Form::label('', 'Username',[ 'class' => 'col-md-4 control-label' ]) }}
                                    
                                    <div class="col-md-6">
                                        {{ Form::text('name', Auth::user()->name, ['class' => 'form-control', 'required' => 'true']) }}

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    {{ Form::label('', 'E-Mail Address',[ 'class' => 'col-md-4 control-label' ]) }}

                                    <div class="col-md-6">
                                        {{ Form::email('email', Auth::user()->email, ['class' => 'form-control', 'required' => 'true']) }}

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{ Form::label('', 'Date of Birth',[ 'class' => 'col-md-4 control-label' ]) }}
                                    <div class="col-md-6">
                                        {{ Form::date('birthday', $admin->birth, ['class' => 'form-control', 'required' => 'true']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{ Form::label('', 'Address',[ 'class' => 'col-md-4 control-label' ]) }}
                                    <div class="col-md-6">
                                        <textarea type="address" name="address" id="address" placeholder="Address" class="form-control">{{ $admin->address }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{ Form::hidden('usertype', 1, ['class' => 'form-control', 'required' => 'true']) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('', 'Phone',[ 'class' => 'col-md-4 control-label' ]) }}
                                    <div class="col-md-6">
                                       {{ Form::text('phone', $admin->phone, ['class' => 'form-control', 'required' => 'true']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{ Form::label('', 'Gender',[ 'class' => 'col-md-4 control-label' ]) }}

                                    @if( $admin->gender === 'male')
                                        <div class="col-md-6"> 
                                            <label class="radio-inline" for="male">
                                                <input type="radio" name="gender" id="male" value="male" checked>
                                              Male
                                            </label> 
                                            <label class="radio-inline" for="female">
                                                <input type="radio" name="gender" id="female" value="female">
                                              Female
                                            </label>
                                        </div>
                                    @else
                                        <div class="col-md-6"> 
                                            <label class="radio-inline" for="male">
                                                <input type="radio" name="gender" id="male" value="male" >
                                              Male
                                            </label> 
                                            <label class="radio-inline" for="female">
                                                <input type="radio" name="gender" id="female" value="female" checked>
                                              Female
                                            </label>
                                        </div>
                                    @endif
                                </div>   
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        {{ Form::submit('Update', ['class' => 'btn btn-primary']) }}
                                        <a href="{{ URL::to('home')}}" class="btn btn-info">
                                            Cancel
                                        </a>

                                    </div>
                                </div>
                            {{ Form::close() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection