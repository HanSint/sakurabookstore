@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" style="margin-top: 70px !important;">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($errors = Session::get('errors'))
                <div class="alert alert-danger">    
                    <ul><li>{{ $errors }}</li></ul>
                </div>
            @endif
            <div class="panel-heading" style="font-size: 18px;">
                UserList
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Usertype</th>
                            <th>Address</th>
                            <th>Phone</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($users as $userlist)
	                      
                           @if($userlist->user_type == 0)
                                <tr class="odd gradeX">
                                    <td>{{$userlist->name}}</td>
                                    <td>{{$userlist->email}}</td>
                                   
                                    <td><ul class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        customer<span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">

                                            <li><a href="{{ URL::to('admin/changeadmin/'.$userlist->id)}}">ChangeAdmin</a> </li>
                                       
                                        </ul>
                                        </ul>
                                    </td>
                                    <td>{{ $userlist->customer->address }}</td>
                                    <td>{{ $userlist->customer->phone }}</td>
                                </tr>
                           @elseif($userlist->user_type == 1)

                                 <tr class="odd gradeX">
                                    <td>{{$userlist->name}}</td>
                                    <td>{{$userlist->email}}</td>
                                    <td><ul class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        admin<span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">

                                            <li><a href="{{ URL::to('admin/removeadmin/'.$userlist->id)}}">RemoveAdmin</a> </li>
                                       
                                        </ul>
                                        </ul>
                                    </td>
                                    <td>{{ $userlist->admin->address }}</td>
                                    <td>{{ $userlist->admin->phone }}</td>
                                </tr>
                            @endif
                                
	                       
	                    @endforeach
	                </tbody>
	            </table>
                
	        </div>
            {{$users->links()}}
	    </div>
	</div>
</div>
@endsection