@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="margin-top: 70px !important;">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel-heading" style="font-size: 16px;">Change Password</div>
                <div class="panel-body">
                @if( Auth::user()->id == 1)    
					{{ Form::open(['url' => 'admin/password/'.Auth::user()->id , 'method' => 'post', 'class'=>'form-horizontal']) }}
          
                        {{ Form::hidden('_method','PUT') }}
                        {{ Form::hidden('_token', csrf_token() ) }}
                    
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Change
                                </button>
                                <a href="{{URL::to('admin/categorylist')}}" class="btn btn-info">Cancle</a>
                            </div>
                        </div>
                    {{ Form::close() }}
                @else
                    {{ Form::open(['url' => 'member/password/'.Auth::user()->id , 'method' => 'post', 'files' => 'true','class'=>'form-horizontal']) }}
          
                        {{ Form::hidden('_method','PUT') }}
                        {{ Form::hidden('_token', csrf_token() ) }}
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Change
                                </button>
                                <a href="{{URL::previous()}}" class="btn btn-info">Cancle</a>
                            </div>
                        </div>
                    {{ Form::close() }}
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

		
	
	