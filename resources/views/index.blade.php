@extends('layouts.app')
@section('content')
<br>
<div id="page-wrapper">
    <div class="row">
        <div style="width:70%;" class="cart">
            <form method="get" class="search_url" action="{{ URL::to('/search')}}" role="form">
                <div id="custom-search-input" style="width:650px;margin-left: 365px;float:left;">
                    <div class="input-group col-md-12">
                        <input type="text" id="search" class="search-query form-control" placeholder="Search" onchange="searched(this)" />
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit" id="btn_search">
                                <span class=" glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </div>
                </div>
            </form>
            <div style="margin-left:1250px;width:100px;margin-top: 50px;">
                <!-- @if(isset($cart_qty))      
                    <a href="{{ URL::to('cart')}}" class="glyphicon glyphicon-shopping-cart">Cart({{ $cart_qty }})</a>     
                @else   
                    <a href="{{ URL::to('cart')}}" class="glyphicon glyphicon-shopping-cart">Cart(0)</a>  
                @endif -->
                @if(Session::has('cart'))           
                    <a href="{{ URL::to('cart')}}" class="glyphicon glyphicon-shopping-cart">Cart({{ $cart_qty }})</a>
                @else 
                    <a href="{{ URL::to('cart')}}" class="glyphicon glyphicon-shopping-cart">Cart(0)</a>    
                @endif
            </div>
        </div>
        <div class="col-md-3 sidemenu"> 
            <div class="nav-side-menu" style="margin-top: 20px;">
                <div class="brand">Categories</div>
                <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
                <div class="menu-list">
                    <ul id="menu-content" class="menu-content collapse out">
                        @foreach($categories as $category)
                        <li data-toggle="collapse" data-target="#{{ str_replace(' ', '', $category->cat_name ) }}" class="collapsed"  aria-expanded="false">
                            <a href="{{ URL::to('category/'.$category->id )}}"><i class="fa fa-gift fa-lg"></i>{{ $category->cat_name }}</a><span class="{{ count($category->childs) != '' ? 'arrow' : '' }}"></span>
                        </li>
                            @if(count($category->childs))
                                <ul class="collapse first" id="{{ str_replace(' ', '', $category->cat_name ) }}">
                                    @include('managechild',['children' => $category->childs])
                                </ul>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="col-md-9 main-menu" style="width:70%;">
            <div class="row"> 
                @foreach($products as $product)
                    <div class="col-lg-3 col-md-6">                   
                        <div>
                             <a href="{{ URL::to('product/'.$product->id)}}">
                                @if(file_exists('../public/photos/'.$product->photos[0]))
                                  <img src="{{asset('photos/'.$product->photos[0])}}" width="200" height="250"/>
                                @else
                                  <img src="{{asset('photos/cover.jpg')}}" width="200" height="250"/>
                                @endif
                                <!-- <img src="{{asset('../public/photos/'.$product->photos[0])}}" width="200" height="250"/> -->
                            </a>                           
                        </div>
                        <br>
                        <div style="width:207px;">
                            <a href="<?= URL::to('session/'.$product->id) ?>" class="btn btn-primary" style="clear: both;">Add To Cart</a>
                            @if(file_exists('../public/pdf/'.$product->pdf))
                                <a href="{{asset('pdf/'.$product->pdf)}}" target="_blank" style="float: right;" class="btn btn-info">Sample PDF</a>
                            @endif
                        </div>
                        <div class="" style="text-align: center;">
                            <h4><b>{{ $product->p_name }}</b></h4>
                        </div>
                        <div style="text-align: center;">
                            {{ $product->price }} kyats
                        </div>                
                        <br>                  
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
