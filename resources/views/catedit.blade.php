@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div  style="margin-top: 70px !important;">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="panel-heading">Category</div>
                    <div class="panel-body">
                        
    					<form class="form-horizontal" role="form" method="POST" action="<?= URL::to('admin/category/'.$category->id)?>">
    						{{ csrf_field()}}
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <input type="hidden" name="cid" value="{{$category->id}}">
                                <label for="category" class="col-md-4 control-label">Category Name</label>

                                <div class="col-md-6">
                                    <input id="category" type="text" class="form-control" name="category" value="{{$category->cat_name}}" required>
                                </div>
                            </div>

                            <div class="form-group">
    			                <label for="phone" class="col-md-4 control-label">Parent Categories</label>
    			                <div class="col-md-6">

                                    <select class=" form-control selectpicker" name="parentid">
                                        <option value="0"></option>
                                        @foreach($allcategory as $categories)
                                            @if($category->parent_id == $categories->id)
                                                <option value="{{$categories->id}}" selected>{{ $categories->cat_name}}</option>
                                            @elseif($category->cat_name != $categories->cat_name)                             
                                                <option value="{{$categories->id}}">{{$categories->cat_name}}</option>
                                            @endif                                        
                                        @endforeach
                                        
                                    </select>

    			                </div>
    			            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                    <a href="{{URL::to('admin/categorylist')}}" class="btn btn-primary">Cancle</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

		
	
	