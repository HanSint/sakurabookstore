<!DOCTYPE html>
<html lang="en">
<head>
    <title>SakuraBookStore</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SakuraBookStore</title>
    <link href="{{ asset ('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('css/metisMenu.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('css/sb-admin-2.css') }}" rel="stylesheet">
    <link href="{{ asset ('css/morris.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!-- <link href="../public/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->

</head>
<body>
    @yield('content')
   <script src="{{ asset ('js/jquery.js') }}"></script> 
   <script src="{{ asset ('js/bootstrap.min.js') }}"></script> 
</body>
</html>