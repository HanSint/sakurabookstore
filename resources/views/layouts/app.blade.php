<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Sakura BookStore') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script> 
    <script src="{{ asset('js/calculate.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pro.js') }}" type="text/javascript"></script>


    <style type="text/css">
        @yield('styles');
        #link{
            font-family:Edwardian Script ITC;
        }
    </style>
</head>
<body>
    <div id="app">
        @if(Auth::user())
            @if(Auth::user()->user_type == 0)
                <nav class="navbar navbar-default navbar-static-top navtop">
            @elseif(Auth::user()->user_type == 1)
                <nav class="navbar navbar-default navbar-static-top navtop" style="background-color:black;">
            @endif
        @else
            <nav class="navbar navbar-default navbar-static-top navtop">
        @endif
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    @if(Auth::user())
                        
                        @if(Auth::user()->user_type == 0)
                        <a class="navbar-brand" href="{{ url('member/home') }}" id="link">
                           <p style="font-size: 20px;color: black;">Sakura BookStore</p>
                        </a>
                        @elseif(Auth::user()->user_type == 1)
                        <div style="margin-top: 10px;">
                            <p style="font-size: 20px;color: white;">Sakura BookStore</p>
                        </div>
                        @endif
                        
                    @else
                        <a class="navbar-brand" href="{{ url('/') }}">
                           <p style="font-size: 20px;color: black;">Sakura BookStore</p>
                        </a>
                    @endif
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                     <!-- <ul class="nav navbar-nav"> -->
                       
                    <!-- </ul> -->
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                            <li><a href="{{ route('login') }}">Checkout</a></li>

                        @else
                            @if(Auth::user()->user_type == 0)
                                <!-- <li><a href="{{ URL::to('home') }}">Home</a></li> -->
                                <li><a href="{{ URL::to('member/orderlist')}}">OrderList</a></li>
                                <li><a href="{{ URL::to('member/checkout')}}">Order</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ URL::to('member/profile/'.Auth::user()->id)}}">Profile</a></li>
                                        <li><a href="{{ URL::to('member/password/')}}">Change Password</a></li>
                                    </ul>
                                </li>
                                <!-- <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a> -->
                                    <!-- <ul class="dropdown-menu" role="menu">
                                        
                                        <form method="post" action="{{ URL::to('user/'.Auth::user()->id) }}">
                                            {{csrf_field()}}
                                            {{method_field('DELETE')}}
                                            <button type="submit" value="delete" style="margin-left:20px;" onclick="return confirm('Are you sure want to remove this item?');">Delete Account</button>
                                            
                                        </form>
                                        <li><a href="{{ URL::to('delete/'.Auth::user()->id) }}">Delete Account</a></li> 
                                   
                                    </ul>
                                </li> -->
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            @elseif(Auth::user()->user_type == 1)
                                <!-- <li><a href="{{ URL::to('home') }}">Home</a></li> -->
                                <!-- <li><a href="{{ URL::to('member/checkout')}}">Order</a></li> -->
                                <!-- <li><a href="{{ URL::to('/admin/userview')}}">UserView</a></li> -->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color:#f26005;;">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ URL::to('/admin/register')}}" style="color:#000;">CreateAdmin</a></li>
                                        <li><a href="{{ URL::to('admin/profile/'.Auth::user()->id )}}" style="color:#000;">Profile</a></li>
                                        <li><a href="{{ URL::to('admin/password/')}}" style="color:#000;">Change Password</a></li> 
                                    </ul>
                                </li>
                                
                                <!-- <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        Order <span class="caret"></span>
                                    </a> -->
                                    <!-- <ul class="dropdown-menu" role="menu"> -->
                                        <li><a href="{{ URL::to('/admin/orderlist')}}" style="color:#f26005;">OrderList</a></li>
                                        <!-- <li><a href="{{ URL::to('member/orderlist')}}">MyOrderList</a></li> -->
                                    <!-- </ul> -->
                                <!-- </li> -->

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color:#f26005;">
                                        Product <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ URL::to('/admin/product') }}" style="color:#000;">CreateProduct</a></li>
                                        <li><a href="{{ URL::to('/admin/productlist')}}" style="color:#000;">Productlist</a></li>
                                    </ul>
                                </li>

                                 <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color:#f26005;">
                                        Category <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ URL::to('/admin/category') }}" style="color:#000;">AddCategory</a></li>
                                        <li><a href="{{ URL::to('/admin/categorylist')}}" style="color:#000;">CategoryList</a></li>
                                    </ul>
                                </li>

                                <!-- <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color:#f26005;">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a> -->

                                    <!-- <ul class="dropdown-menu" role="menu">
                                        <form method="post" action="{{ URL::to('user/'.Auth::user()->id) }}">
                                            {{csrf_field()}}
                                            {{method_field('DELETE')}}
                                            <button type="submit" value="delete" style="margin-left:20px;" onclick="return confirm('Are you sure want to remove this item?');">Delete Account</button>                                            
                                        </form> -->
                                        <!-- <li><a href="{{ URL::to('user/'.Auth::user()->id) }}">Delete Account</a></li> -->
                                    <!-- </ul> -->
                                <!-- </li> -->
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();" style="color:#f26005;">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                               
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
    </div>
    
        <div class="modal-footer">
        </div>
        
</body>
</html>
