@extends('layouts.app')

@section('content')

<div class="container">
	<!-- <div  style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">  -->
	<div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default"  style="margin-top: 70px !important;">
        	@if (count($errors) > 0)
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <div class="panel-heading">
                <div class="panel-title">Add New Book</div>
            </div>  
            <div class="panel-body" >
            {{ Form::open(['url' => 'admin/product', 'method' => 'post', 'files' => 'true','class'=>'form-horizontal']) }}
                          
		            {{ csrf_field()}}
		            <div class="form-group">
		            	{{ Form::label('', 'Product Name',[ 'class' => 'col-md-4 control-label' ]) }}
		                <div class="col-md-6">
		                	{{ Form::text('p_name','', ['class' => 'form-control', 'placeholder' => 'Product Name','autofocus'=>'true' , 'required' => 'true']) }}				             
		                </div>
		            </div>
		            <div class="form-group">
		            	{{ Form::label('', 'Price',[ 'class' => 'col-md-4 control-label']) }}
		              
		                <div class="col-md-6">
		                	{{ Form::text('price','', ['class' => 'form-control', 'placeholder' => 'Price' , 'required' => 'true']) }}
		                </div>
		            </div>
		            <div class="form-group">
		            	{{ Form::label('', 'Quantity',[ 'class' => 'col-md-4 control-label']) }}
		                <div class="col-md-6">
		                    {{ Form::text('qty','', ['class' => 'form-control', 'placeholder' => 'Quantity' , 'required' => 'true']) }}
		                </div>
		            </div>
		            <div class="form-group">
		            	{{ Form::label('', 'Description',[ 'class' => 'col-md-4 control-label']) }}
		                
		                <div class="col-md-6">
		                	{{ Form::text('description','', ['class' => 'form-control', 'placeholder' => 'Description' , 'required' => 'true']) }}
		                </div>
		            </div>				    
				    <div class="form-group">
		            	{{ Form::label('', 'Author',[ 'class' => 'col-md-4 control-label']) }}
		                
		                <div class="col-md-6">
		                	{{ Form::text('author','', ['class' => 'form-control', 'placeholder' => 'Author Name' , 'required' => 'true']) }}
		                </div>
		            </div>
		      
		            <div class="form-group">
		            	{{ Form::label('', 'Categories',[ 'class' => 'col-md-4 control-label']) }}
		                <div class="col-md-6">
		                    <select class="form-control selectpicker" name="cid">
								@foreach($categories as $category)
					                <li>
					                   <option value="{{$category->id}}">{{$category->cat_name}}</option>
					                    @if(count($category->childs))
					                        @include('managechild',['parents' => $category->childs])
					                    @endif
					                </li>
					            @endforeach
							</select>

		                </div>
		            </div>
		            <label for="additional" style="margin-left:115px;">Additional Info</label>
		            	
		            <div  id="infos" style="width:500px;">
			            <div class="form-group field" style="width:500px;margin-left: 60px;">
			            	<div class="col-md-4">
			            	{{ Form::text('label[]','', ['class' => 'form-control','style'=>'margin-left:7px' ,'placeholder' => 'Label' , 'required' => 'true']) }}
			            
			                </div>
			                <div class="col-md-6">
			                	{{ Form::text('info[]','', ['class' => 'form-control', 'placeholder' => 'Additional Info' , 'required' => 'true']) }}
			                </div>

			               	<a class="btn btn-primary btn-sm" id="add" onclick="add()"> + </a>
			               	<!-- <a class="btn btn-danger btn-sm" id="del" > x </a> -->
			            </div>
			 
			        </div>

		            <div class="form-group">
		            	<label class="col-md-4 control-label">Add Photos</label>
		            	<div class="col-md-6">
		               		<input type="file" name="file[]"  multiple>
		               	</div>				        
		            </div>
		              	   
		            <div class="form-group">
		            	<label class="col-md-4 control-label">Add Sample PDF</label>
		            	<div class="col-md-6">
		               		<input type="file" name="pdf"  multiple>
		               	</div>				        
		            </div>
		              	          
		            <div class="form-group">
		                <div class="col-md-6 col-sm-offset-3">
		                	{{ Form::submit('Create', ['class' => 'btn btn-primary']) }} 
		                </div>
		            </div>
				{!! Form::close() !!}
			</div><!-- pannel body -->
		</div> <!-- panel -->
	</div> <!-- style -->
</div> <!-- container --> 
<script type="text/javascript">
	
	function add(){

		$('#infos').append('<div class="form-group field"  style="width:500px;margin-left:60px;"><div class="col-md-4"><input type="text" style="margin-left:7px" class="form-control" name="label[]" value="" placeholder="label"></div><div class="col-md-6"><input type="text" name="info[]" placeholder="Additional" class="form-control"></div><a class="btn btn-primary btn-sm" id="add" onclick="add()"> + </a> <a class="btn btn-danger btn-sm del" id="del"> x </a></div>');
	}

	$('body').delegate('#del','click',function(){
		
		$(this).parent().remove();
	})
	

</script>
@endsection