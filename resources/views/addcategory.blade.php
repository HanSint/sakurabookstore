@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div  style="margin-top: 70px !important;">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="font-size: 16px;">Add Category</div>
                    <div class="panel-body">
                        {{ Form::open(['url' => '/admin/category', 'method' => 'post','class'=>'form-horizontal']) }}
    					
                            <div class="form-group">
                                {{ Form::label('', 'Category Name',['class' => 'col-md-4 control-label']) }}
                                <div class="col-md-6">
                                    {{ Form::text('category','', ['class' => 'form-control', 'placeholder' => 'Category','required'=>'true']) }}
                                </div>
                            </div>

                            <div class="form-group">
                            {{ Form::label('', 'Parent Categories',['class' => 'col-md-4 control-label']) }}
    			                <div class="col-md-6">
    			                    <select class="form-control selectpicker" class="col-md-4 control-label" name="parentid">
    			                    	<option value="0" selected></option>
                                         @foreach($categories as $category)
                                            <li>
                                               <option value="{{$category->id}}">{{$category->cat_name}}</option>
                                                @if(count($category->childs))
                                                    @include('managechild',['parents' => $category->childs])
                                                @endif
                                            </li>
                                        @endforeach
                                        
    								</select>

                                   
    			                </div>
    			            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
                                    <a href="{{URL::to('admin/categorylist')}}" class="btn btn-primary">Cancle</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

		
	
	