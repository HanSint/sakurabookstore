@extends('layouts.app')
@section('content')
<div id="page-wrapper">
    <div class="col-md-3 sidemenu" style="margin-top: 50px !important;margin-left: -15px;"> 
        <div class="nav-side-menu">
            <div class="brand">Categories</div>
            <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
            <div class="menu-list">
                <ul id="menu-content" class="menu-content collapse out">
                    @foreach($categories as $category)
                    <li data-toggle="collapse" data-target="#{{ str_replace(' ', '', $category->cat_name ) }}" class="collapsed"  aria-expanded="false">
                        <a href="{{ URL::to('category/'.$category->id )}}"><i class="fa fa-gift fa-lg"></i>{{ $category->cat_name }}</a><span class="{{ count($category->childs) != '' ? 'arrow' : '' }}"></span>
                    </li>
                        @if(count($category->childs))
                            <ul class="collapse first" id="{{ str_replace(' ', '', $category->cat_name ) }}">
                                @include('managechild',['children' => $category->childs])
                            </ul>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div  style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"> 
        <!-- <div class="panel panel-info"> -->
            <!-- <div class="panel-heading">
                <div class="panel-title">Product Detail</div>
            </div> -->

            <div class="panel-body" style="width: 1000px;"> 
                <form id="signupform" class="form-horizontal" role="form"> 
                    <div style="width:100px;float: right;" class="cart">
                            @if(Auth::user())
                                @if(isset($carts))
                                    <a href="{{ URL::to('member/cart')}}" class="glyphicon glyphicon-shopping-cart">Cart({{ $carts }})</a> 
                                @else         
                                    <a href="{{ URL::to('member/cart')}}" class="glyphicon glyphicon-shopping-cart">Cart(0)</a>
                                @endif
                            @else
                                 @if(Session::has('cart'))      
                                    <a href="{{ URL::to('cart')}}" class="glyphicon glyphicon-shopping-cart">Cart({{ $carts }})</a>     
                                @else   
                                    <a href="{{ URL::to('cart')}}" class="glyphicon glyphicon-shopping-cart">Cart(0)</a>  
                                @endif
                            @endif
                    </div> 
                    <div style="width: 630px;">        
            	        <div style="width: 250px;float: left;">
                            @for($i=0; $i<count($detail->photos); $i++)
                                @if(file_exists('../public/photos/'.$detail->photos[$i]))
                                  <img src="{{asset('photos/'.$detail->photos[$i])}}" width="220" height="280"/>
                                @else
                                  <img src="{{asset('photos/cover.jpg')}}" width="200" height="250"/>
                                @endif
            	               <!-- <img src="{{ asset('../public/photos/'.$detail->photos[$i]) }}" width="150" height="150" /> -->
                            @endfor
            	        </div>
            	        <div style="width: 360px;float:right;">
                            <p style="font-size: 20px;">
                	            <b>  {{ $detail->p_name }}  </b>
                	        </p>
                            <br>
                            <p>
                                Author       :   {{ $detail->author }}
                            </p>
                	        <p>
                	            Price        :  {{ $detail->price }} kyats
                	        </p>   
                	        <div>
                                @for($i=0; $i<count($detail->additional_info); $i++)
                                    @foreach($detail->additional_info[$i] as $key => $value)
                	        	      {{ $key }} : {{ $value }} 
                                    @endforeach
                                     <p></p>
                                @endfor
                	        </div> 
                            <p>
                                Description  :   {{ $detail->description }}
                            </p>                        

                        @if(Auth::user())
                            @if(Auth::user()->user_type == 0)
                                <br>
                                <a href="{{ URL::to('cart/'.$detail->id)}}" class="btn btn-primary">Add To Cart</a>
                                @if(file_exists('../public/pdf/'.$detail->pdf))
                                    <a href="{{asset('pdf/'.$detail->pdf)}}" target="_blank"  class="btn btn-info">Sample PDF</a>
                                @endif 
                                <br><br>
                                <a href="{{URL::to('member/home')}}" class="btn btn-success">Continue Shopping</a> 
                                               
                            @elseif(Auth::user()->user_type == 1)
                                <br>
                                <a href="{{ URL::previous() }}" class="btn btn-info">Back</a>
                                <a href="{{ URL::to('admin/product/'.$detail->id)}}" class="btn btn-primary">Edit</a>
                            @endif
                        @else
                            <br>
                            <a href="{{ URL::to('session/'.$detail->id)}}" class="btn btn-primary">Add To Cart</a>
                            @if(file_exists('../public/pdf/'.$detail->pdf))
                                <a href="{{asset('pdf/'.$detail->pdf)}}" target="_blank" class="btn btn-info">Sample PDF</a>
                            @endif 
                            <br><br>
                            <a href="{{URL::to('index')}}" class="btn btn-success">Continue Shopping</a>
                        @endif
                        </div>
                    </div>
                </form>
            </div>
	    <!-- </div> -->
    </div>
</div>

@endsection