@extends('layouts.app')
@section('content')
<div class="container">
	<div  style="margin-top:50px;" class="col-sm-8 col-sm-offset-2"> 
        <div class="panel panel-default" style="margin-top: 70px !important;">
            <div class="panel-heading">
                <div class="panel-title">OrderDetail</div>
            </div>  
            <div class="panel-body" >
                <form id="signupform" class="form-horizontal" role="form" method="post" action="<?= URL::to('admin/product_update') ?>">         
			        <div>
			        	<b>Name</b> : {{ $order->name}}
			        </div>
			         <div>
			        	<b>Address</b> : {{ $order->address}}
			        </div>
			         <div>
			        	<b>Phone</b> : {{ $order->phone}}
			        </div>
			        <div>
			        	<b>Billing</b> : {{ $order->billing}}
			        </div>
			        <div>
			        	<b>Shipping</b> : {{ $order->shipping}}
			        </div>
			        <hr>
			        
			        @foreach($orderdetails as $order)
			        <div style="width:700px;height:150px;">
			        	<div style="width:150px;float:left;">
				        		<img src="{{asset('photos/'.$order->product->photos[0])}}" width="120" height="130"/>
				        </div>
				        <div style="width:550px;float:right;">
				        	<div>
					        	<b>ProductName</b>   : {{ $order->product->p_name }}
					        </div>
					        <div>
					        	<b>ProductPrice</b> : {{ $order->product->price }}
					        </div>
					        <div>
					          	<b>Quentity</b>    :    {{ $order->order_qty }}
					        </div>
				        </div>
			        </div> 
			        <br>
		            @endforeach
		             <div class="form-group">
		                <div class="col-md-6 col-sm-offset-3" style="margin-left:10px;">
		                    <a href="{{URL::previous()}}" class="btn btn-info">Back</a>
		                </div>
		            </div>
				</form> 
		</div> <!-- panel -->
	</div> <!-- style -->
</div> <!-- container -->  
</div>
@endsection