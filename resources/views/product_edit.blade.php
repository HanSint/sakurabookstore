@extends('layouts.app')
@section('content')

<div class="container">
	<div style="margin-top: 70px !important;">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	<div  style="margin-top:50px;" class="col-md-8 col-md-offset-2"> 
        <div class="panel panel-default" >
            <div class="panel-heading">
                <div class="panel-title">Edit Product</div>
            </div>  
            <div class="panel-body" >
            	{{ Form::open(['url' => 'admin/product/'.$product->id , 'method' => 'post', 'files' => 'true','class'=>'form-horizontal']) }}
          
		            {{ Form::hidden('_method','PUT') }}
		            {{ Form::hidden('_token', csrf_token() ) }}
		            <div class="form-group">
			             @if(isset($product->photos))
			            	@for($i=0; $i<count($product->photos); $i++)
			            		<img src="{{asset('photos/'.$product->photos[$i])}}" width="150" height="150" style="margin:12px;" />
			            	@endfor
			            @endif
		            </div>
		           
		            <div class="form-group">
	                  	{{ Form::label('', 'Product Name',[ 'class' => 'col-md-4 control-label' ]) }}
	                    <div class="col-md-6">
	                      	{{ Form::text('p_name', $product->p_name, ['class' => 'form-control', 'placeholder' => 'Product Name','required' => 'true']) }}                     
	                    </div>
	                </div>

		            <div class="form-group">
	                  	{{ Form::label('', 'Price',[ 'class' => 'col-md-4 control-label']) }}
	                  
	                    <div class="col-md-6">
	                      	{{ Form::text('price', $product->price , ['class' => 'form-control', 'placeholder' => 'Price' , 'required' => 'true']) }}
	                    </div>
	                </div>
		         
		            <div class="form-group">
	                  	{{ Form::label('', 'Quantity',[ 'class' => 'col-md-4 control-label']) }}
	                    <div class="col-md-6">
	                        {{ Form::text('qty', $product->qty, ['class' => 'form-control', 'placeholder' => 'Quantity' , 'required' => 'true']) }}
	                    </div>
	                </div>
		            
		            <div class="form-group">
	                  	{{ Form::label('', 'Description',[ 'class' => 'col-md-4 control-label']) }}
	                    
	                    <div class="col-md-6">
	                      {{ Form::text('description', $product->description , ['class' => 'form-control', 'placeholder' => 'Description' , 'required' => 'true']) }}
	                    </div>
	                </div>
		      
			        <div class="form-group">
	                  	{{ Form::label('', 'Author',[ 'class' => 'col-md-4 control-label']) }}
	                    <div class="col-md-6">
	                        {{ Form::text('author', $product->author, ['class' => 'form-control', 'placeholder' => 'Author' , 'required' => 'true']) }}
	                    </div>
	                </div>

		            <div class="form-group">
		                <label for="phone" class="col-md-4 control-label">Categories</label>
		                <div class="col-md-6">
		                    <select class="form-control selectpicker" name="cid">
		                    	@foreach($categories as $category)
		                    		@if($product->category_id == $category->id)
		                    			<option value="{{$category->id}}" selected>{{ $category->cat_name}}</option>
		                    		@else
							  		<option value="{{$category->id}}">{{$category->cat_name}}</option>
							  		@endif
								@endforeach
							</select>

		                </div>
		            </div>
		            <label for="additional" style="margin-left:115px;">Additional Info</label>
		            	
		            <!-- <div class="thumbnail" style="width:460px;" id="infos"> -->
		            <div  id="infos" style="width:500px;">
		             	@for($i=0; $i<count($product->additional_info); $i++)
                            @foreach($product->additional_info[$i] as $key => $value)
	        	        	    <div class="form-group field" style="width:500px;margin-left: 60px;">
					            	
					                <div class="col-md-4">
				                    	{{ Form::text('label[]', $key , ['class' => 'form-control','style'=>'margin-left:7px' ,'placeholder' => 'Label' , 'required' => 'true']) }}
				                  
				                    </div>
				                    <div class="col-md-6">
				                        {{ Form::text('info[]', $value , ['class' => 'form-control', 'placeholder' => 'Additional Info' , 'required' => 'true']) }}
				                    </div>

					               	<a class="btn btn-primary btn-sm" id="add" onclick="add()"> + </a>
					               	<a class="btn btn-danger btn-sm" id="del" > x </a>
					            </div>
                            @endforeach
                        @endfor 
			        </div>
		           <div class="form-group">
		            	<label class="col-md-4 control-label">Add Photos</label>
		            	<div class="col-md-6">
		               		<input type="file" name="file[]" value="" multiple>
		               	</div>				        
		            </div>
		            <div class="form-group">
		            	<label class="col-md-4 control-label">Sample PDF</label>
		            	<div class="col-md-6">
		               		<input type="file" name="pdf" value="">
		               	</div>				        
		            </div>
		            <div class="form-group">
		                <div class="col-md-6 col-sm-offset-3">
		                	{{ Form::submit('Update', ['class' => 'btn btn-primary']) }}
		        			
		                    <a href="{{ URL::to('admin/productlist')}}" class="btn btn-success" >Cancel</a>
		                </div>
		            </div>
				{{ Form::close() }}
			</div><!-- pannel body -->
		</div> <!-- panel -->
	</div> <!-- style -->
	</div>
</div> <!-- container -->  
<script type="text/javascript">
	
	function add(){

		$('#infos').append('<div class="form-group field"  style="width:500px;margin-left: 60px;"><div class="col-md-4"><input type="text" style="margin-left:7px" class="form-control" name="label[]" value="" placeholder="label"></div><div class="col-md-6"><input type="text" name="info[]" placeholder="Additional" class="form-control"></div><a class="btn btn-primary btn-sm" id="add" onclick="add()"> + </a> <a class="btn btn-danger btn-sm del" id="del"> x </a></div>');
	}

	$('body').delegate('#del','click',function(){
		$(this).parent().remove();
		var news = $("#infos").find(".field").html();
		if(news == undefined){
			add();
		}
		
	})
	

</script>
@endsection