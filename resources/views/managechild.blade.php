@if(isset($childs))
	<ul>
	@foreach($childs as $child)
		<li>
		    <form method="post" action="{{URL::to('admin/category/'.$child->id)}}">{{csrf_field()}} {{ $child->cat_name }} <input type="hidden" name="_method" value="DELETE" ><button type="submit" class="btn btn-warning btn-sm" onclick="return confirm('Are You Sure Want To Delete?');"> Del </button>  <a href="{{URL::to('admin/category/'.$child->id)}}" class="btn btn-primary btn-sm">Edit</a> </form>
		    <br>
		@if(count($child->childs))
	            @include('managechild',['childs' => $child->childs])
	        @endif
		</li>
	@endforeach
	</ul>

@elseif(isset($parents))
	<ul>
	@foreach($parents as $child)
		<li>
		   <option value="{{$child->id}}">---{{$child->cat_name}}</option>
			@if(count($child->childs))
	            @include('managechild',['parents' => $child->childs])
	        @endif
		</li>
	@endforeach
	</ul>
	
@elseif(isset($children))
	<ul>
		@foreach($children as $child)
			<li data-toggle="collapse" data-target="#{{ str_replace(' ', '', $child->cat_name ) }}" class="collapsed"  aria-expanded="false"><a href="{{ URL::to('category/'.$child->id )}}"><i class="fa fa-angle-double-right"></i>{{ $child->cat_name }}</a><span class="{{ count($child->childs) != '' ? 'fa fa-angle-double-down' : '' }}" style="margin-left: 199px;"></span></li>

			@if(count($child->childs))
				<ul class="sub-menu collapse" id="{{ str_replace(' ', '', $child->cat_name ) }}">
		        	@include('managechild',['children' => $child->childs])
		        </ul>
		    @endif
			
		@endforeach
	</ul>
@endif