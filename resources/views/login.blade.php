@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                           <strong>{{ $message }}</strong>
                    </div>
                @endif
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    {{ Form::open(['url' => 'route('login')', 'method' => 'post', 'class'=>'form-horizontal']) }}
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            {{ Form::label('','E-Mail Address',[ 'class' => 'col-md-4 control-label'])}}

                            <div class="col-md-6">
                                {{ Form::email('email','',['class' => 'form-control', 'placeholder' => 'Email','autofocus'=>'true' , 'required' => 'true'])}}

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            {{ Form::label('','Password',['class' => 'col-md-4-control-label']) }}

                            <div class="col-md-6">
                                {{ Form::password('password','',['class'=> 'form-control', 'placeholder' => 'Password' , 'required' => 'true'])}}

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        {{ Form::checkbox('remember','')}}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">

                                {{ Form::submit('Login',['class'=> 'btn btn-primary'])}}
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>

                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
