<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class category extends Model
{
    // use SoftDeletes;
    
    protected $fillable = [
        'cat_name', 'parent_id',
    ];

    // protected $dates = ['deleted_at'];

    public function childs() {
        return $this->hasMany('App\Category','parent_id','id') ;
    }

    public function parent()
    {
        return $this->belongsTo('App\Category','parent_id','id');
    }

    public function child() {
        $child = array();
       foreach($this->id as $ids) {
            $child []= $ids ;
        }
        return $child;
    }

}
