<?php

namespace App\Http\Controllers;

use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Auth;
use DB;
use Query;
use App\Category;
use App\Product;
use App\Cart;
use Session;

class CategoryController extends Controller
{
    //view category list
    public function category_list() {

        //retrieve all data from the category table
    	$allcategory = Category::all();
        //retrieve parent_id=0 data     	
    	$categories = Category::where('parent_id', '=', 0)->get();
    	
        return view('categorylist',compact('allcategory','categories'));       
    }

    //view add category blade
    public function addcategory(){
        //retrieve parent_id=0 data for show parent category
    	$category = Category::where('parent_id', '=', 0)->get();
    	
        return view('addcategory',['categories'=> $category]);  
    }

    //add category
    public function cat_create(Request $request){

        $this->validate($request,[
            'category' => 'required',
        ]);
    	
        //insert category table
		$category = new Category;
    	$category->cat_name = request('category');
    	$category->parent_id = request('parentid');
    	$category->save();

        return redirect('admin/category')->with('success','Category Create Successful.'); 
    	
    }

    //edit category
    public function cat_edit($id){

        //retrieve all data from the category table
    	$allcategory = Category::all();
        //retrieve a data according to paramenter id
    	$category = Category::where('id',$id)->first();
    
    	return view('catedit',compact('category','allcategory'));

    }

    //update category
    public function cat_update($id,Request $request) {

        $this->validate($request,[
            'category' => 'required',
        ]);

        //update category according to $id
    	$category = Category::find($id);
    	$category->cat_name = $request->get('category');
    	$category->parent_id = $request->get('parentid');
    	$category->save();

    	return redirect('admin/categorylist')->with('success', 'Category Update Successfully.');

    }

    //remove category(if you delete parent category that all child will delete )
    public function remove_category($id) {

        //retrieve data according to paramenter id
        $cats = Category::where('parent_id',$id)->get();

        //delete data according to paramenter id
        Category::where('id',$id)->delete();
        
        //paramenter id is parent when all child delete 
        foreach ($cats as $key => $cat) {

            $id = $cat['id'] ;
            // Category::where('id',$id)->delete();

            //recusive (called this functioin again and again untill childs does not exist) 
            CategoryController::remove_category($id);

        }

        return redirect('admin/categorylist')->with('success', 'Category Delete Successfully.');
    }
    
    //if select an category show this page  
    public function items($id) {
        
        $products = Product::where('category_id',$id)->get();

        $categories = Category::where('parent_id', '=', 0)->get();
        $cart_qty = 0;
        $carts = Session::get('cart');
        if($carts){
            /*sum array column value not loop */
            $cart = array_sum(array_column($carts, 'order_qty'));
        }
        if(Auth::user()){
            
            $carts = Cart::where('user_id', Auth::user()->id)->get();
            $cart = count($carts);
            return view('subproduct',compact('products','categories','cart'));
        }
        
        return view('subproduct',compact('products','categories','cart'));
       
    }
}
