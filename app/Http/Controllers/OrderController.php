<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Orderdetail;
use App\Customer;
use App\User;
use Auth;

class OrderController extends Controller
{
    public function orderlist() {

    	$orders = Order::paginate(3);
    	$orders->load('user');

    	return view('orderlist',compact('orders'));
    }

    public function orderdetail($id) {

    	$orderdetail = Orderdetail::where('order_id',$id)->get();
    	$orderdetail->load('product');
    	
        $order  = Order::where('id',$id)->first();
		
    	return view('orderdetail',['order' => $order ,'orderdetails' => $orderdetail]);    	 
    }

    public function memberorderlist() {
       
        $userorders = Order::where('user_id',Auth::user()->id)->paginate(3);
        $userorders->load('user');

        return view('orderlist',compact('userorders'));

    }
}
