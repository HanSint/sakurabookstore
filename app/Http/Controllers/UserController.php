<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Customer;
use App\Product;
use App\Category;
use App\Admin;
use DB;

class UserController extends Controller
{
    //userlist
    public function userview() {
        //retrieve all data from user table
        $users = User::paginate(5);
        $users->load('customer');  //retation customer table
        $users->load('admin');     //retation admin table

        return view('userview',compact('users'));
    }

    //profile blade show
    public function profile($id){

        //retrieve data 
    	$customer = Customer::where('user_id', $id )->first();   	
    	return view('profile',['customer'=> $customer]);
    }

    //ptofile update
    public function profile_update($id,Request $request){

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            // 'birthday' => 'required',
            // 'address' => 'required|string|max:255',
            // 'gender' => 'required',
            // 'phone'=> 'required|numeric',
        ]);

        //retrieve data from user table according to paramenter id
        $userid = User::where('id',$id)->first();
        
        //update insert from user table
    	$user = User::find($id);
    	$user->name = request('name');
        $user->email = request('email');
        $user->password = $userid->password;
        $user->user_type = request('usertype');
        $user->save();

        //update insert from customer table
        $customer = Customer::where('user_id',$id)->first();
        $customer->address = "No23,123street,Tamwe Township,Yangon";
        $customer->gender = "male";
        $customer->birth = "1990-10-30";
        $customer->phone = "098776555";
        $customer->user_id = $user->id;
        $customer->save();

        return redirect('member/profile/'.$id)->with('success', 'Update Profile successfully.');
    }

    //delete account
    public function delete($id){

        //if user id= 1 this account does not delete 
        if($id != 1){

            //if customer ,user and customer tables data delete(id) 
           if(Auth::user()->user_type == 0){
                User::where('id',$id)->delete();
                Customer::where('user_id',$id)->delete();
           }
           //if admin, user and admin tables data delete(id)
           elseif(Auth::user()->user_type == 1) {
                User::where('id',$id)->delete();
                Admin::where('user_id',$id)->delete();
           } 
           return redirect('home')->with('success', 'Delete Profile successfully.');
        } 
        else {
            return redirect('home')->with('errors', 'Access Denied.');
        }
    }

    //change customer to admin 
    public function changeadmin($id) {

        //update one column to user table
        User::where('id',$id)->update(['user_type'=> 1]);

        $customer = Customer::where('user_id',$id)->first();

        $admin = new Admin;
        $admin->address = $customer->address;
        $admin->gender = $customer->gender;
        $admin->birth = $customer->birth;
        $admin->phone = $customer->phone;
        $admin->user_id = $id;
        $admin->save();

        //delete data from customer table and this data insert into admin table 
        //using forcedelete() because use softDelete 
        Customer::where('user_id',$id)->forceDelete();

        return redirect('admin/userview')->with('success','Change Admin Successful');
    }

    //change admin to customer
    public function removeadmin($id) {

        //if userid = 1 admin to customer does not change
        if($id != 1){

            //update user_type column  change 1 to 0
            User::where('id',$id)->update(['user_type'=> 0]);

            $admin = Admin::where('user_id',$id)->first();

            $customer = new Customer;
            $customer->address = $admin->address;
            $customer->gender = $admin->gender;
            $customer->birth = $admin->birth;
            $customer->phone = $admin->phone;
            $customer->user_id = $id;
            $customer->save();

            //delete data from admin table and this data insert into customer table 
            //use forcedelete() because using softDelete 
            Admin::where('user_id',$id)->forceDelete();

            return redirect('admin/userview')->with('success','Remove Admin Successful');
        } else {

           return redirect('admin/userview')->with('errors','Access Denied'); 

        }
    }

    //password change blade show
    public function password_change() {

        return view('password');
    }

    //admin password change
    public function password($id,Request $request) {

        $this->validate($request,[
            'password' => 'required|string|min:6|confirmed',
        ]);


        $user = User::where('id',$id)->first();
      
        User::where('id',$id)->update(['password'=> bcrypt(request('password'))]);

        return redirect('admin/password')->with('success','Change Password Successful');
    }
        
}