<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Order;
use App\Orderdetail;
use App\Cart;
use Carbon\Carbon;

class OrderdetailController extends Controller
{
   //save order to order table and orderdetail table
    public function order(Request $request) {

      $this->validate($request,[
         'pid' => 'required',
         'qty' => 'required',
         'name' => 'required',
         'address' => 'required',
         'billing' => 'required',
         'phone' => 'required',
         'total' => 'required',
         'shipping' => 'required',
         
      ]);

		// $date = Date('Y-m-d');
      $date = Carbon::now();
     
      //save order to order table
		$order = new Order;
		$order->user_id = Auth::user()->id;
		$order->total = request('total') ;
      $order->name = request('name');
      $order->address = request('address');
      $order->billing = request('billing');
      $order->shipping = request('shipping');
      $order->phone = request('phone');
		$order->date = $date->toDateString();
		$order->save();
      
      //create an array
      $orders = [];
      for($i=0; $i<count(request('pid')); $i++){

      	$orders[$i]	= [

            'product_id' => request('pid')[$i],
      		'order_qty' => request('qty')[$i],        		
      		'order_id' => $order->id,
         ];
      }
      Orderdetail::insert($orders);

      //delete data from cart table all of this user data
		Cart::where('user_id',Auth::user()->id)->delete();

		return redirect('member/home')->with('success','Order Successful');

	}
}
