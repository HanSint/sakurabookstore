<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Admin;
use App\User;
use DB;

class AdminController extends Controller
{
    public function admin_register(){

        return view('adminregister');
    }

    public function create_register(Request $request){

        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            // 'birthday' => 'required',
            // 'address' => 'required|string|max:255',
            // 'gender' => 'required',
            // 'phone'=> 'required|numeric',
        ]);

        $user = new User;
        $user->name = request('name');
        $user->email = request('email');
        $user->password = bcrypt(request('password'));
        $user->user_type = request('usertype');
        $user->save();

        $admin = new Admin;
        $admin->address = "No23,162street,Tamwe Township,Yangon";
        $admin->gender = "male";
        $admin->birth = "1990-11-30";
        $admin->phone = "098726223";
        $admin->user_id = $user->id;
        $admin->save();

        return redirect('admin/userview')->with('success', 'Create Admin successfully.');

    }
    public function profile($id){
  	
    	$admin = Admin::where('user_id', $id )->first(); 
    	return view('profile',['admin'=> $admin]);
        
    } 

    public function admin_update($id,Request $request){

        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'birthday' => 'required',
            'address' => 'required|string|max:255',
            'gender' => 'required',
            'phone'=> 'required|numeric',
        ]);


        $userid = User::where('id',$id)->first();

    	$user = User::find($id);
    	$user->name = request('name');
        $user->email = request('email');
        $user->password = $userid->password;
        $user->user_type = request('usertype');
        $user->save();

        $admin = admin::where('user_id',$id)->first();
        $admin->address = request('address');
        $admin->gender = request('gender');
        $admin->birth = request('birthday');
        $admin->phone = request('phone');
        $admin->user_id = $user->id;
        $admin->save();

        return redirect('admin/userview')->with('success', 'Update Profile successfully.');
        
    }

}
