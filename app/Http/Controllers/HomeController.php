<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Cart;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::all();
        $allcategory = Category::all();
        $category = Category::where('parent_id', '=', 0)->get();

//no of item count for to show cart
        // this is all item retrieve for one user using userid andthen count number of row
        // $cart = Cart::where('user_id', Auth::user()->id)->get();
        // $carts = count($cart);

        // this is retrieve sum of qty only 
        $carts = Cart::where('user_id', Auth::user()->id)->sum('qty');

        return view('home',['cart'=>$carts,'products'=> $product,'categories' => $category]);
    }
    
}
