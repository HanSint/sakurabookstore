<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Customer;
use App\Product;
use App\Category;
use App\Cart;
use Auth;
use DB;
use Session;

class ProductController extends Controller
{
    public function index(Request $request) {

        $products = Product::all();
        $categories = Category::where('parent_id', '=', 0)->get();
        $cart_qty = 0;
        $cart = Session::get('cart');
        if($cart){
            for($i=0;$i<count($cart);$i++){
                    $cart_qty += $cart[$i]['order_qty'];
            }
            /*sum array column value not loop */
            // $cart_qty = array_sum(array_column($cart, 'order_qty'));
        }
        return view('index',['products'=> $products,'categories' => $categories,'cart_qty' => $cart_qty]);
    }

    public function product_list(){

        $products = Product::paginate(8);
        return view('productlist',['products' => $products]);
    }

    public function product() {

        // $category = Category::all();
        $category = Category::where('parent_id', '=', 0)->get();
        return view('product',['categories'=> $category]);

    }

    public function product_create(Request $request) {   

        $this->validate($request, [
            'p_name' => 'required|string|max:255',
            'price' => 'required|numeric',
            'qty' => 'required|numeric',
            'description' => 'required',
            'label' => 'required',
            'info' => 'required',
            'author' => 'required',
            'file' => 'required',
            'cid' => 'required',
        ]);
        
        $files = request('file');
        $infos = request('info');
        $label = request('label');
        // dd($infos,$label);/
        
        for($i=0; $i<count($infos); $i++){
            $info[] = array(request('label')[$i]=>request('info')[$i]);
        }

        for($i=0; $i<count($files); $i++){

            $file = request('file')[$i];
            $filename = time().$file->getClientOriginalName();
            $file->move('../public/photos', $filename);

            $photos[] = $filename;
        }
        $pdf = request('pdf');
        if($pdf){
            $pdfname = time().$pdf->getClientOriginalName();
            $pdf->move('../public/pdf', $pdfname);
            $pdf = $pdfname;
        }

        $product = new Product;
        $product->p_name = request('p_name');
        $product->price = request('price');
        $product->qty = request('qty');
        $product->description = request('description');
        $product->additional_info = $info;
        $product->author = request('author');
        $product->photos = $photos;
        $product->pdf = $pdf;
        $product->category_id = request('cid');
        $product->save();

       return redirect('admin/productlist')->with('success', 'Product Create successfully.');

    }

    public function product_detail($id) {

     	$detail = Product::where('id',$id)->first();
        $categories = Category::where('parent_id', '=', 0)->get();

        if(Auth::user()){
             $carts = Cart::where('user_id', Auth::user()->id)->sum('qty');
        }else{
            $carts = 0;
            $cart = Session::get('cart');
            if($cart){
                for($i=0;$i<count($cart);$i++){
                    $carts += $cart[$i]['order_qty'];
                }
                /*sum array column value not loop */
                // $carts = array_sum(array_column($cart, 'order_qty'));
            }
        }

     	return view('product_detail' , compact('detail','categories','carts') );
    }

    public function product_edit($id){

     	$product =  Product::where('id',$id)->first();

     	$categories = Category::all();
     	
     	return view('product_edit' , compact('product', 'categories' ) );	
    }

    public function product_update(Request $request,$id){

        $this->validate($request, [
            'p_name' => 'required|string|max:255',
            'price' => 'required|numeric',
            'qty' => 'required|numeric',
            'description' => 'required',
            'author' => 'required',
            'label' => 'required',
            'info' => 'required',
            'cid' => 'required',
        ]);

   		$files = request('file');

        $product = Product::find($id);

        if($files != null) {                   
            for($i=0; $i<count($files); $i++){

                $file = request('file')[$i];
                $filename = time().$file->getClientOriginalName();
                $file = $file->move('../public/photos', $filename);

                $photos[] = $filename;
                $product->photos =  $photos;
            } 
        }
        
        $pdf = request('pdf');
        if($pdf){
            $pdfname = time().$pdf->getClientOriginalName();
            $pdf->move('../public/pdf', $pdfname);
            $pdf = $pdfname;
            $product->pdf = $pdf;
        }

        $infos = request('info');
        
        for($i=0; $i<count($infos); $i++){
            $info[] = array(request('label')[$i]=>request('info')[$i]);
        }

    	
    	$product->p_name = request('p_name');
        $product->price = request('price');
        $product->qty = request('qty');
        $product->description = request('description');
        $product->author = request('author');
        $product->additional_info = $info;
       
        $product->category_id = request('cid');
        $product->save();

        return redirect('admin/productlist')->with('success', 'Product Edit successfully.');

    }
    public function product_delete($id) {
        
        Product::where('id',$id)->delete();
        return redirect('admin/productlist')->with('success', 'Delete Product successfully.');

    }

    public function search($name){
        // var_dump($name);
        $products = Product::where('p_name', 'like', '%'.$name.'%')
                        ->orwhere('author', 'like' , '%'.$name.'%')->get();

        $categories = Category::where('parent_id', '=', 0)->get();
        $cart_qty = 0;
        $cart = Session::get('cart');
        if($cart){
            for($i=0;$i<count($cart);$i++){
                    $cart_qty += $cart[$i]['order_qty'];
            }
            /*sum array column value not loop */
            // $cart_qty = array_sum(array_column($cart, 'order_qty'));
        }
       
        return view('index',['products'=> $products,'categories' => $categories,'cart_qty' => $cart_qty]);
    }
}
