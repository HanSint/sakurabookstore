<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\User;

class CustomerController extends Controller
{
    public function password($id,Request $request) {
    	
        $user = User::where('id',$id)->first();
 
        User::where('id',$id)->update(['password'=> bcrypt(request('password')) ]);

        return redirect('member/password')->with('success','Change Password Successful');
           
    }
}
