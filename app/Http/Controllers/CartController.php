<?php

namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Session;
use Auth;
use DB;
use App\Cart;
use App\Product;


class CartController extends Controller
{
  //add cart
 	public function addcart($id) {

    //retrieve same product_id and same user_id form cart tb using user_id and product_id
    $carts = Cart::where('user_id',Auth::user()->id)->where('product_id',$id)->first();

    //new data is add when same data is not exist 
    if($carts == null) //check same data exist or not
    {
      //insert into database
      $cart = new Cart;
      $cart->user_id = Auth::user()->id;
      $cart->product_id = $id;
      $cart->qty = 1;
      $cart->save();
    }
    else{

      // $cart = Cart::find($carts->id);
      if(isset($_GET['qty'])) {
        //same data is exist update qty only
        $carts->qty = $_GET['qty'] ;
        $carts->save();
      }
      else {
        $carts->qty = $carts->qty+1 ;
        $carts->save();
      }
     
    }
  return redirect()->back();
 		// return redirect('home');
 	}

  //cartlist view
 	public function cartview() {

    //retrieve cart from database
 		$carts = Cart::where('user_id', Auth::user()->id)->get();
 		$carts->load('product'); //relation product table

 		return view('cartview',compact('carts'));
 	}

  //delete one product
 	public function cartremove($id) {

    //delete a product from database
 		DB::table('carts')
		->where('user_id',Auth::user()->id)
		->where('id',$id)
		->delete();

 		return redirect('member/cart')->with('success','Remove Item Successful');
 	}
  
  //delete all product to the cart
 	public function cartclear() {
    
    //delete all product from cart table
 		Cart::where('user_id',Auth::user()->id)->delete();

 		return redirect('member/cart')->with('success','All Items Remove Successful');
 	}

  //retrieve all product from the cart  
 	public function checkout() {

    //retrieve all product from cart table but does not duplicate
    // $carts = Cart::distinct()->where('user_id',Auth::user()->id)->groupby('product_id')->get(['product_id']);

 		$carts = Cart::where('user_id',Auth::user()->id)->get();
 		$carts->load('product');
    
    $carts_qty = Cart::where('user_id', Auth::user()->id)->sum('qty');

 		return view('checkout',['carts'=> $carts,'carts_qty' => $carts_qty]);
 	}

//session for guest customer

  //guest add cart 
  public function cart($id) {

    //retrieve data from the product table according to paramenter id
    $product = Product::where('id',$id)->first();

    $cart = Session::get('cart');

    if($cart != null)
    {
      for($i=0; $i<count($cart); $i++){
        if($cart[$i]['id'] == $id){
          if(isset($_GET['qty'])){
            $cart[$i]['order_qty'] = $_GET['qty'];
          }else{
            $qty = 1;
            $cart[$i]['order_qty'] = $cart[$i]['order_qty'] + $qty;
          }
          return redirect()->back();
          // return redirect('index');
        }
      }
      $product['order_qty'] = 1;
      Session::push('cart',$product);
    }
    else
    {
      $product['order_qty'] = 1;
      Session::push('cart',$product);
    }

    /* this is column value search in array not loop*/
    // $key =array_search($id,array_column($cart, 'id'));
   return redirect()->back();
    // return redirect('index');
  }

 //view cart 
  public function guestcart() {
  
    //retrieve all data from the session
    $products = Session::get('cart');

    return view('cartview',compact('products'));

  }

  //delete all items into the cart
  public function allremove() {

    //delete all data from the session
    Session::forget('cart');

    return redirect('cart')->with('success','Remove Item Successful');
  }

  //delete an item into the cart
  public function guestdelete($id) {

    //id is array index no
    //delete all data from the sessioin 
    $products = session()->pull('cart');
    $cart = [];
    for($i=0; $i<count($products); $i++){
      if($i != $id){
        $cart[] = $products[$i];
      }
    }
    session()->put('cart', $cart);

    /* check delete value exist this session 
     search ( delete array no ) */

    // $key =array_search($id,array_column($products, 'id'));

    // if($key !== false) {
    //   //delete a value from the sessiion
    //   unset($products[$key]);

    //   $j = count($products)+1;
    //   for($i=0; $i<$j; $i++){

    //     if(isset($products[$i])){

    //       $pro[] = $products[$i];

    //     }          
    //   }
    //   //insert into session
    //   session()->put('cart', $pro);
    // }
    // else {
      // session()->put('cart', $cart);
    // }

    return redirect('cart')->with('success','Remove Item Successful');
  }   
}
