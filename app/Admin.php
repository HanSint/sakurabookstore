<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class admin extends Model
{	
	// use SoftDeletes;
	
	protected $fillable = [
        'address', 'gender', 'birth','phone','user_id',
    ];

    // protected $dates = ['deleted_at'];

    public function user(){
    	return $this->belongsTo(User::class);
    }
}
