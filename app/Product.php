<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class product extends Model
{
	// use SoftDeletes;

    protected $fillable = [
        'pname', 'price','qty','description','additional_info','author','photos','category_id',
    ];

    protected $casts = [
    'photos' => 'array',
    'additional_info' => 'array',
	];

	// protected $dates = ['deleted_at'];
	
	public function orderDetail() {
		return $this->hasOne(orderDetail::class);
	}

	public function cart() {
		return $this->hasMany(Cart::class);
	}
}
