<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class customer extends Model
{
	// use SoftDeletes;

    protected $fillable = [
        'address', 'gender', 'birth','phone','user_id',
    ];

    // protected $dates = ['deleted_at'];

    public function user() {
    	return $this->belongsTo(User::class);
    }

    public function order() {
    	return $this->hasMany(Order::class);
    }
}
