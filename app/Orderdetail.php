<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class orderdetail extends Model
{
	// use SoftDeletes;

	protected $fillable = [
		'id','product_id','order_qty','name','address','order_id','billing','shipping','is_deleted',
	];

	protected $casts = [
	    'product_id' => 'array',
	    'order_qty' => 'array',
	];

	// protected $dates = ['deleted_at'];

    public function Product() {
    	return $this->belongsTo(Product::class);
    }

    public function Order() {

    	return $this->belongsTo(Order::class);
    }
}
