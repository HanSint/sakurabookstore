<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Admin;
use App\Product;
use App\Category;
use App\Customer;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt(111111),
                'user_type' => '1',
            ],
            [
                'name' => 'user',
                'email' => 'user@gmail.com',
                'password' => bcrypt(111111),
                'user_type' => '0',
            ]
        ]);

        Admin::create([

            'address' => '152street,3,tamwe',
            'gender' => 'female',
            'birth' => '1993-11-30',
            'phone' => '0998765443',
            'user_id' => '1',
        ]);

        Customer::create([

            'address' => '162street,3,tamwe',
            'gender' => 'female',
            'birth' => '1994-10-20',
            'phone' => '0998765443',
            'user_id' => '2',
        ]);

       DB::table('categories')->insert([
            [
                'cat_name' => 'Information Technology',
                'parent_id' => 0,
            ],
            [
                'cat_name' => 'Graphic',
                'parent_id' => 1,
            ],
            [
                'cat_name' => 'Software',
                'parent_id' => 1,
            ],
            [
                'cat_name' => 'Technical',
                'parent_id' => 1,
            ],
            [
                'cat_name' => 'Cooking',
                'parent_id' => 0,
            ],
            [
                'cat_name' => 'Novel Books',
                'parent_id' => 0,
            ]
        ]);

       // DB::table('products')->insert([
       //      [
       //          'p_name' => 'A Kyi Taw',
       //          'price' => 1200,
       //          'qty' => 10,
       //          'description' => 'for happy',
       //          'additional_info' => '100',
       //          'author' => 'A Kyi Taw',
       //          'photos' => 'happy.png',
       //          'category_id' => '6',
       //      ],
       //      [
       //          'p_name' => 'Lon Htar Htar',
       //          'price' => 1200,
       //          'qty' => 10,
       //          'description' => 'for drama',
       //          'additional_info' => '100',
       //          'author' => 'Lon Htar Htar',
       //          'photos' => 'drama.png',
       //          'category_id' => '6',
       //      ],
       //      [
       //          'p_name' => 'Jue',
       //          'price' => 1200,
       //          'qty' => 10,
       //          'description' => 'for love',
       //          'additional_info' => '100',
       //          'author' => 'Jue',
       //          'photos' => 'love.png',
       //          'category_id' => '6',
       //      ],
       //      [
       //          'p_name' => 'Mya Than Tint',
       //          'price' => 1200,
       //          'qty' => 10,
       //          'description' => 'for life',
       //          'additional_info' => '100',
       //          'author' => 'Mya Than Tint',
       //          'photos' => 'life.png',
       //          'category_id' => '6',
       //      ],
       //      [
       //          'p_name' => 'U Phone (Dar Du)',
       //          'price' => 1200,
       //          'qty' => 10,
       //          'description' => 'for psycho',
       //          'additional_info' => '100',
       //          'author' => 'U Phone (Dar Du)',
       //          'photos' => 'psycho.png',
       //          'category_id' => '6',
       //      ],
       //      [
       //          'p_name' => 'Professional Web Developer',
       //          'price' => 10000,
       //          'qty' => 10,
       //          'description' => 'for study php programming',
       //          'additional_info' => '100',
       //          'author' => 'Ei Maung(Fairway Technology)',
       //          'photos' => 'php.png',
       //          'category_id' => '3',
       //      ],
       //      [
       //          'p_name' => 'Wut Yee Cooking Book',
       //          'price' => 10000,
       //          'qty' => 10,
       //          'description' => 'for cooking myanmar curry',
       //          'additional_info' => '100',
       //          'author' => 'Wut Yee',
       //          'photos' => 'food.png',
       //          'category_id' => '5',
       //      ]
       //  ]);

       
    }
}
