$(document).ready(function(){

	pricetotal();
	subtotal();
	township();

	$('body').delegate('#qty','keyup mouseup',function(){

	var tr = $(this).parent().parent();
	var item = tr.find('#qty').val();
	var price = tr.find('#price').find('p').text();
	var amount = item*price;
	tr.find('#pricetotal').text(amount);
	subtotal();      
	})
});

function township(){
	var town = $('#town').val();
 	$('#shipping').text(town);
 	$('.shipping').val(town);
 	subtotal();
}

function pricetotal() {

	var tr = $(this).parent().parent();
	var item = tr.find('#qty').val();
	var price = tr.find('#price').find('p').text();
	var amount = item*price;
	tr.find('#pricetotal').text(amount);
	subtotal();      
}

function subtotal(){
	var stotal = 0;
	$('.pricetotal').each(function(k,v){
		var total = $(this).text()-0;
		stotal += total;
	})
	$('#subtotal').text(stotal);
	var tax = (stotal/100)*10 ;
	$('#tax').text(tax)-0;
	$('.tax').val(tax);
	var shipping = $('#town').val()-0;
	var total = (tax + stotal + shipping);
	$('#total').text(total);
	$('.total').val(total);

}
function deleteRow(btn) {
	var row = btn.parentNode.parentNode;
	row.parentNode.removeChild(row);
	subtotal();
}