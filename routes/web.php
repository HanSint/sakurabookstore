<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/','ProductController@index');

Route::get('/index', 'ProductController@index');

Route::get('/search/{name}', 'ProductController@search');

Route::delete('/user/{id}','UserController@delete');




Route::get('/cart','CartController@guestcart');

Route::get('/cart/{id}','CartController@addcart');

Route::delete('/cart','CartController@cartclear');

Route::delete('/cart/{id}','CartController@cartremove');

Route::get('/product/{id}', 'ProductController@product_detail');

Route::get('/category/{id}','CategoryController@items');

Route::get('/session/{id}','CartController@cart'); 

Route::get('/session','CartController@cartget');

Route::delete('/session/{id}','CartController@guestdelete');

Route::delete('/session','CartController@allremove');

Route::get('/search/{name}','ProductController@search');

//member

Route::group(array('prefix'=>'member','middleware'=> 'auth'),function(){
	Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/cart','CartController@guestcart');

	Route::get('/cart/{id}','CartController@addcart');

	Route::delete('/cart','CartController@cartclear');

	Route::delete('/cart/{id}','CartController@cartremove');


    Route::get('/product/{id}', 'ProductController@product_detail');

	Route::get('/profile/{id}','UserController@profile');

	Route::put('/profile/{id}','UserController@profile_update');


	Route::get('/orderlist','OrderController@memberorderlist');

	Route::get('/order/{id}','OrderController@orderdetail');

	Route::post('/order','OrderdetailController@order');
		

	Route::get('/password','UserController@password_change');

	Route::put('/password/{id}','CustomerController@password');

	Route::get('/cart','CartController@cartview');

	Route::get('/checkout','CartController@checkout');
	
});

//admin
Route::group(array('prefix'=>'admin','middleware'=> 'admin'), function(){

	// Route::get('/userview','UserController@userview');

	Route::get('/register','AdminController@admin_register');

	Route::get('/profile/{id}','AdminController@profile');

	Route::put('/profile/{id}','AdminController@admin_update');

	Route::post('/register','AdminController@create_register');



	Route::get('/productlist','ProductController@product_list');

	Route::get('/product','ProductController@product');

	Route::get('/product/{id}' , 'ProductController@product_edit');

	Route::get('/product_detail/{id}', 'ProductController@product_detail');

	Route::post('/product','ProductController@product_create');

	Route::put('/product/{id}' , 'ProductController@product_update');

	Route::delete('product/{id}','ProductController@product_delete');



	Route::get('/categorylist', 'CategoryController@category_list');

	Route::get('/category','CategoryController@addcategory');

	Route::get('/category/{id}','CategoryController@cat_edit');

	Route::post('/category','CategoryController@cat_create');

	Route::put('/category/{id}','CategoryController@cat_update');

	Route::delete('/category/{id}','CategoryController@remove_category');



	Route::get('/orderlist','OrderController@orderlist');

	Route::get('/order/{id}','OrderController@orderdetail');

	// Route::get('/cart','CartController@cartview');


	// Route::get('/changeadmin/{id}','UserController@changeadmin');

	// Route::get('/removeadmin/{id}','UserController@removeadmin');



	Route::get('/password','UserController@password_change');

	Route::put('/password/{id}','UserController@password');
	
});


